#　おかずがちゃ

ご飯を決めるのがめんどくさいときに回すガチャ


## インストール

```
$ python setup.py install
```

## 使い方

```
$ cd gacha
$ FLASK_APP=main.py flask run --host=0.0.0.0
```

実行後、ブラウザからlocalhost:5000にアクセス

## おかずを増やす

### jsonを直接編集する
data/recipesにファイルを追加すればガチャのおかずを増やすことができる

```
{
  "name": おかずの名前(string)
  "genre": おかずのジャンル。"和", "洋", "中"のいずれか(string)
  "main": 主菜か副菜か。"main", "side"のいずれか(string)
  "ingredients": 主な食材の種類。"meat", "fish", "vegetable", "other"のいずれか(string)
  "URL": レシピの参考URL(list(string))
}
```

### レシピ追加UIから追加する

