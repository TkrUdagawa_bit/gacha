# -*- coding:utf-8 -*- 
from flask import Flask, render_template, request
from gacha.gacha import gacha
import json
import os
import glob

app = Flask(__name__)

def add_recipe_file(data_dir="data/recipes",
                    filename=None,
                    name=None,
                    genre=None,
                    main=None,
                    ingredients=None,
                    url=None):
    with open(os.path.join(data_dir, filename), "w") as f:
        recipe_data = {
            "name": name,
            "genre": genre,
            "main": main,
            "ingredients": ingredients,
            "URL": url
        }
        json.dump(recipe_data, f, indent=4)

@app.route("/")
def top():
    return render_template("index.html")

@app.route("/gacha/")
def gacha_top():
    return render_template("gacha.html")

@app.route("/gacha_result/", methods=["POST"])
def gacha_result():
    genre = request.form["genre"]
    if genre == "None":
        genre = None
    main = request.form["main"]
    if main == "None":
        main = None
    ingredients = request.form["ingredients"]
    if ingredients == "None":
        ingredients = None
    num = request.form["num"]
    result = gacha(data_dir="data/recipes",
                   genre=genre,
                   main=main,
                   ingredients=ingredients,
                   num_items=int(num))
    results = [json.load(open(f)) for f in result]
    return render_template("gacha_result.html", results=results)

@app.route("/dish/")
def dish():
    return render_template("dish.html")
    
@app.route("/recipe/")
def recipe():
    filepath = os.path.join("data", "recipes", "*.json")
    files = glob.glob(filepath)
    results = [json.load(open(f)) for f in files]
    return render_template("all_recipes.html", results=results)


@app.route("/recipe/add/")
def add_recipe():
    return render_template("add_recipe.html")

@app.route("/recipe/add/done/", methods=["POST"])
def add_recipe_done():
    name = request.form["name"]
    genre = request.form["genre"]
    mains = request.form["main"]
    ingredients = request.form["ingredients"]
    url = request.form["url"].split(",")
    filename = "{}.json".format(name)
    add_recipe_file(data_dir="data/recipes",
                    filename=filename,
                    name=name,
                    genre=genre,
                    main=mains,
                    ingredients=ingredients,
                    url=url)
    return render_template("add_recipe_done.html")
