# -*- coding:utf-8 -*-

import random
import os
import glob
import json

def filter_result(result, genre=None, main=None, ingredients=None):
    json_dict = {}
    result_dict = {}
    
    for f in result:
        json_dict[f] = json.load(open(f, "r"))

    for d in json_dict:
        if genre is not None:
            if json_dict[d]["genre"] != genre:
                continue
            
        if ingredients is not None:
            if json_dict[d]["ingredients"] != ingredients:
                continue
                
        if main is not None:
            if json_dict[d]["main"] != main:
                continue
            
        result_dict[d] = json_dict[d]
        
    return list(result_dict.keys())

def gacha(data_dir="data/recipes",
          genre=None,
          main=None,
          ingredients=None,
          num_items=1):
    filepath = os.path.join(data_dir, "*.json")
    files = glob.glob(filepath)
    files = filter_result(files, genre, main,
                          ingredients)
    random.shuffle(files)
    return files[:num_items]

if __name__ == "__main__":
    print(gacha())
    print(gacha(num_items=5))
    print(gacha(genre="和", num_items=5))
    print(gacha(main="main", num_items=5))
